package org.ricardorm13;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;


public class HelloLambda implements RequestHandler<RequestEvent, ResponseEvent> {

    @Override
    public ResponseEvent handleRequest(RequestEvent requestEvent, Context context) {
        return new ResponseEvent(requestEvent);
    }
}
