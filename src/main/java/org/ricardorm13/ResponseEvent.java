package org.ricardorm13;

public class ResponseEvent {
    private String responseEvent;

    public ResponseEvent(RequestEvent req){
        this.setResponseEvent("Olá " + req.getNome() + "! você está em " + System.getenv("AWS_REGION"));
    }


    public String getResponseEvent() {
        return responseEvent;
    }

    public void setResponseEvent(String responseEvent) {
        this.responseEvent = responseEvent;
    }
}
